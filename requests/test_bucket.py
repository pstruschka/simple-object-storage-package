import requests
import json

from bucket import init_test_bucket, clear_bucket

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes.OK
STATUS_BAD = requests.codes.BAD
STATUS_NOT_FOUND = requests.codes.NOT_FOUND


def test_bad_bucket():
    resp = requests.get(BASE_URL + '/2a*-a?create')
    assert resp.status_code == STATUS_BAD
    resp = requests.get(BASE_URL + '/abc.abc?create')
    assert resp.status_code == STATUS_BAD
    resp = requests.get(BASE_URL + '/a+a?create')
    assert resp.status_code == STATUS_BAD

    
def test_create_bucket():
    (bucket, bucket_path) = init_test_bucket('bucket', create=False)
    
    resp = requests.post(bucket_path + '?create')
    assert resp.status_code == STATUS_OK
    assert 'created' in resp.json()
    assert 'modified' in resp.json()
    assert 'name' in resp.json()
    assert resp.json()['name'] == bucket
    resp = requests.post(bucket_path + '?create')
    assert resp.status_code == STATUS_BAD
    clear_bucket(bucket)
    
def test_delete_bucket():
    (bucket, bucket_path) = init_test_bucket('bucket')

    resp = requests.delete(bucket_path + '?delete')
    assert resp.status_code == STATUS_OK
    resp = requests.delete(bucket_path + '?delete')
    assert resp.status_code == STATUS_BAD

    
def test_list_bucket():
    (bucket, bucket_path) = init_test_bucket('bucket')

    resp = requests.get(bucket_path + '?list')
    assert resp.status_code == STATUS_OK
    assert 'created' in resp.json()
    assert 'modified' in resp.json()
    assert 'name' in resp.json()
    assert resp.json()['name'] == bucket
    assert 'objects' in resp.json()
    assert resp.json()['objects'] == []
    clear_bucket(bucket)
    resp = requests.get(bucket_path + '?list')
    assert resp.status_code == STATUS_BAD

    
