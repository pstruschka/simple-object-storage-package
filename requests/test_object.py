import requests
import json

from bucket import init_test_bucket, clear_bucket

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes.OK
STATUS_BAD = requests.codes.BAD
STATUS_NOT_FOUND = requests.codes.NOT_FOUND

def test_create_object():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + '.file?create')
    assert resp.status_code == STATUS_BAD
