import requests
import json

BASE_URL = 'http://127.0.0.1:5000'

def clear_bucket(bucket):
    bucket_path = BASE_URL + '/' + bucket
    requests.delete(bucket_path + '?delete')


def init_test_bucket(bucket, create = True):
    bucket_path = BASE_URL + '/' + bucket
    clear_bucket(bucket)
    if create:
        requests.post(bucket_path + '?create')
    return bucket, bucket_path
