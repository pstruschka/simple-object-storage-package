import requests

from bucket import init_test_bucket, clear_bucket

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes.OK
STATUS_BAD = requests.codes.BAD
STATUS_NOT_FOUND = requests.codes.NOT_FOUND

SMALL_FILE_PATH = './files/64KB.bin'
SMALL_FILE_MD5 = 'f75f17065aaba61e48bf57b0243bf960'
SMALL_FILE_URL = '/64KB.bin'

def test_get_metadata():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    
    with open(SMALL_FILE_PATH, 'rb') as f:
        requests.put(url=bucket_path + SMALL_FILE_URL + '?partNumber=1',
                            data=f.read(),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': SMALL_FILE_MD5})
    requests.post(bucket_path + SMALL_FILE_URL + '?complete')
    resp = requests.get(bucket_path + "/file?metadata")
    assert resp.status_code == STATUS_NOT_FOUND
    
    resp = requests.get(bucket_path + SMALL_FILE_URL + '?metadata')
    assert resp.status_code == STATUS_OK
    assert resp.json() == {}

def test_put_metadata():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    
    with open(SMALL_FILE_PATH, 'rb') as f:
        requests.put(url=bucket_path + SMALL_FILE_URL + '?partNumber=1',
                            data=f.read(),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': SMALL_FILE_MD5})
    requests.post(bucket_path + SMALL_FILE_URL + '?complete')
    resp = requests.put(url=bucket_path + "/file?metadata&key=key",
                        data="value")
    assert resp.status_code == STATUS_NOT_FOUND
    
    resp = requests.put(url=bucket_path + SMALL_FILE_URL +"?metadata&key=key",
                        data="value")
    assert resp.status_code == STATUS_OK
    
    resp = requests.get(bucket_path + SMALL_FILE_URL + '?metadata')
    assert resp.status_code == STATUS_OK
    assert 'key' in resp.json()
