import requests
import json
import hashlib

from bucket import init_test_bucket, clear_bucket

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes.OK
STATUS_BAD = requests.codes.BAD
STATUS_NOT_FOUND = requests.codes.NOT_FOUND
FILE_PATH = './files/256MB.bin'
FILE_NAME = '256MB.bin'
FILE_MD5 = '167664ac7913b2fdb5e1e63f83c59fd5'
FILE_URL = '/256MB.bin'
FILE_SIZE = 268435456

parts = [
    ("./files/bin.00", "429093d4c02874f9923ff331ac02d8f3", 104857600),
    ("./files/bin.01", "ec2e978edbed2cf6d2e9590ddd016da7", 104857600),
    ("./files/bin.02", "fadcbb715d2bd1bc8d57fef83db394c1", 58720256)
]

SMALL_FILE_PATH = './files/64KB.bin'
SMALL_FILE_NAME = '64KB.bin'
SMALL_FILE_MD5 = 'f75f17065aaba61e48bf57b0243bf960'
SMALL_FILE_URL = '/64KB.bin'
SMALL_FILE_SIZE = 65536

def test_create_object():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + '/.a?create')
    assert resp.status_code == STATUS_BAD
    resp = requests.post(bucket_path + FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    resp = requests.get(bucket_path + '?list')
    assert resp.json()['objects'] == []
    clear_bucket(bucket)

def test_complete_object():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    with open(FILE_PATH, 'rb') as f:
        resp = requests.put(url=bucket_path + FILE_URL + '?partNumber=0',
                            data=f.read(5),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': FILE_MD5})
        assert resp.status_code == STATUS_BAD
        assert 'md5' in resp.json()
        assert 'length' in resp.json()
        assert 'partNumber' in resp.json()
        assert 'error' in resp.json()
        assert resp.json()['error'] == 'InvalidPartNumber'

    with open(FILE_PATH, 'rb') as f:
        resp = requests.put(url=bucket_path + FILE_URL + '?partNumber=1',
                            data=f.read(),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': FILE_MD5})
        assert resp.status_code == STATUS_OK
        assert 'md5' in resp.json()
        assert resp.json()['md5'] == FILE_MD5
        assert 'length' in resp.json()
        assert resp.json()['length'] == FILE_SIZE
        assert 'partNumber' in resp.json()
        assert resp.json()['partNumber'] == 1
        assert 'error' not in resp.json()
    resp = requests.post(bucket_path + FILE_URL + '?complete')
    assert resp.status_code == STATUS_OK
    assert 'eTag' in resp.json()
    assert 'length' in resp.json()
    assert resp.json()['length'] == FILE_SIZE
    assert 'name' in resp.json()
    assert resp.json()['name'] == FILE_NAME

    resp = requests.get(bucket_path + '?list')
    assert len(resp.json()['objects']) == 1
    clear_bucket(bucket)

def test_partial_upload():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    for (i, (path, md5, size)) in enumerate(parts):
        with open(path, 'rb') as f:
            resp = requests.put(url=bucket_path + FILE_URL + '?partNumber=' + str(i + 1),
                                data=f.read(),
                                headers={'Content-Type': 'application/octet-stream',
                                         'Content-MD5': md5})
            print(resp.content)
            assert resp.status_code == STATUS_OK
            assert 'md5' in resp.json()
            assert resp.json()['md5'] == md5
            assert 'length' in resp.json()
            assert resp.json()['length'] == size
            assert 'partNumber' in resp.json()
            assert resp.json()['partNumber'] == i + 1
            assert 'error' not in resp.json()
    resp = requests.post(bucket_path + FILE_URL + '?complete')
    assert resp.status_code == STATUS_OK
    assert 'eTag' in resp.json()
    assert 'length' in resp.json()
    assert resp.json()['length'] == FILE_SIZE
    assert 'name' in resp.json()
    assert resp.json()['name'] == FILE_NAME
    clear_bucket(bucket)

def test_download():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    with open(SMALL_FILE_PATH, 'rb') as f:
        resp = requests.put(url=bucket_path + SMALL_FILE_URL + '?partNumber=1',
                            data=f.read(),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': SMALL_FILE_MD5})
        assert resp.status_code == STATUS_OK
        assert 'md5' in resp.json()
        assert resp.json()['md5'] == SMALL_FILE_MD5
        assert 'length' in resp.json()
        assert resp.json()['length'] == SMALL_FILE_SIZE
        assert 'partNumber' in resp.json()
        assert resp.json()['partNumber'] == 1
        assert 'error' not in resp.json()
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?complete')
    assert resp.status_code == STATUS_OK
    assert 'eTag' in resp.json()
    assert 'length' in resp.json()
    assert resp.json()['length'] == SMALL_FILE_SIZE
    assert 'name' in resp.json()
    assert resp.json()['name'] == SMALL_FILE_NAME

    resp = requests.get(bucket_path + SMALL_FILE_URL)
    assert hashlib.md5(resp.content).hexdigest() == SMALL_FILE_MD5
    clear_bucket(bucket)

def test_download_range():
    (bucket, bucket_path) = init_test_bucket('bucket')
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?create')
    assert resp.status_code == STATUS_OK
    with open(SMALL_FILE_PATH, 'rb') as f:
        resp = requests.put(url=bucket_path + SMALL_FILE_URL + '?partNumber=1',
                            data=f.read(),
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': SMALL_FILE_MD5})
        assert resp.status_code == STATUS_OK
        assert 'md5' in resp.json()
        assert resp.json()['md5'] == SMALL_FILE_MD5
        assert 'length' in resp.json()
        assert resp.json()['length'] == SMALL_FILE_SIZE
        assert 'partNumber' in resp.json()
        assert resp.json()['partNumber'] == 1
        assert 'error' not in resp.json()
    resp = requests.post(bucket_path + SMALL_FILE_URL + '?complete')
    assert resp.status_code == STATUS_OK
    assert 'eTag' in resp.json()
    assert 'length' in resp.json()
    assert resp.json()['length'] == SMALL_FILE_SIZE
    assert 'name' in resp.json()
    assert resp.json()['name'] == SMALL_FILE_NAME

    hash_md5 = hashlib.md5()

    resp = requests.get(url=bucket_path + SMALL_FILE_URL,
                        headers={"Range": "bytes=-" + str(SMALL_FILE_SIZE//2)})
    hash_md5.update(resp.content)
    resp = requests.get(url=bucket_path + SMALL_FILE_URL,
                        headers={"Range": "bytes="+ str(SMALL_FILE_SIZE//2 + 1) + "-"})
    hash_md5.update(resp.content)
    assert hash_md5.hexdigest() == SMALL_FILE_MD5
    clear_bucket(bucket)
    
